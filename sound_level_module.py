import numpy as np

import os
import time
import template

CHUNKSIZE = 22050  # fixed chunk size
RATE = 22050

time.sleep(5)

class SoundLevel(template.Module):
    def __init__(self):
        super().__init__()
        self.use('microphone')
        print('Using microphone')
        # noise window
        data = self.read_new('microphone')
        while data is None:
            data = self.read_new('microphone')
        self.noise_sample = np.frombuffer(data, dtype=np.float32)[-10000:]
        
        self.loud_factor = int(os.getenv('LOUD_FACTOR'))
        self.loud_threshold = np.mean(np.abs(self.noise_sample)) * self.loud_factor
        print("Loud threshold", self.loud_threshold)

        self.count = 0
        self.count_threshold = int(os.getenv('COUNT_THRESHOLD'))
        self.start_timer = time.time()
        self.time_out = int(os.getenv('TIME_OUT_SECONDS'))

        self.current_window = None

    def run(self):
        while True:
            # Read chunk and load it into numpy array.
            data = self.read_new('microphone')
            while data is None:
                data = self.read_new('microphone')
            self.current_window = np.frombuffer(data, dtype=np.float32)
            if (np.mean(np.abs(self.current_window)) < self.loud_threshold):
                print("Inside silence reign")
            else:
                print("Inside loud reign")
                # a loud noise has been detected
                self.count += 1
                elapsed_time = time.time() - self.start_timer
                if elapsed_time > self.time_out:
                    if self.count >= self.count_threshold:
                        self.send_message('count', self.count)                        

                        self.count = 0

                    # reset vars
                    self.count = 0
                    self.start_timer = time.time()

SoundLevel().run()

