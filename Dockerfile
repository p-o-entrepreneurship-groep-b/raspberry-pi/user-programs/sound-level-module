FROM python:3.7-alpine
RUN apk --no-cache add musl-dev linux-headers g++
WORKDIR /template
COPY template/requirements.txt requirements.txt
RUN pip install -r requirements.txt
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
WORKDIR /template
COPY template .
WORKDIR /code
COPY . .
CMD ["python3.7", "sound_level_module.py"]

